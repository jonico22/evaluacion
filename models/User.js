const mongoose = require('mongoose');

const User = new mongoose.Schema({
  nombre: {
    type: String,
    required: true,
  },
  apellido_paterno: {
    type: String,
    required: true,
  },
  apellido_materno: {
    type: String,
    required: true,
  },
  edad: {
    type: Number,
    required: false,
  },
  email: {
    type: String,
    required: false,
  },
  image: {
    type: String,
    required: false,
  },
  fecha_registro: {
    type: Date,
    required: false,
  },
  estado: {
    type: Boolean,
    default: true,
  },
});

module.exports = mongoose.model('user', User);
