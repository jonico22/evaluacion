const mongoose = require('mongoose');

const Dish = new mongoose.Schema({
  categoria_id: {
    type: String,
    required: false,
  },
  nombre: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: false,
  },
  precio: {
    type: Number,
    required: true,
  },
  descripcion: {
    type: String,
    required: false,
  },
  estado: {
    type: Boolean,
    default: true,
  },
});

module.exports = mongoose.model('dish', Dish);
