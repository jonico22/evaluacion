const mongoose = require('mongoose');

const Category = new mongoose.Schema({
  nombre: {
    type: String,
    required: true,
  },
  descripcion: {
    type: String,
    required: false,
  },
  estado: {
    type: Boolean,
    default: true,
  },
});

module.exports = mongoose.model('category', Category);
