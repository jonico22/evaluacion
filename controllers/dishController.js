const Dish = require('../models/Dish');

const DishController = {
  list() {
    return new Promise((resolve, reject) => {
      Dish.find((error, list) => {
        if (error) reject(error);
        resolve(list);
      });
    });
  },

  store(categoria_id, nombre, image, precio, descripcion) {
    return new Promise((resolve, reject) => {
      Dish.create(
        {
          categoria_id,
          nombre,
          image,
          precio,
          descripcion,
        },
        (error, newDish) => {
          if (error) reject(error);
          resolve(newDish);
        }
      );
    });
  },
};

module.exports = DishController;
