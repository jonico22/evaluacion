const User = require('../models/User');

const userController = {
  list() {
    return new Promise((resolve, reject) => {
      User.find((error, list) => {
        if (error) reject(error);
        resolve(list);
      });
    });
  },

  store(nombre, apellido_paterno, apellido_materno, edad, email, image) {
    return new Promise((resolve, reject) => {
      User.create(
        {
          nombre,
          apellido_paterno,
          apellido_materno,
          edad,
          email,
          image,
        },
        (error, newCategory) => {
          if (error) reject(error);
          resolve(newCategory);
        }
      );
    });
  },
};

module.exports = userController;
