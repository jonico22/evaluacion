const Category = require('../models/Category');

const CategoryController = {
  list() {
    return new Promise((resolve, reject) => {
      Category.find((error, list) => {
        if (error) reject(error);
        resolve(list);
      });
    });
  },

  store(nombre, descripcion) {
    return new Promise((resolve, reject) => {
      Category.create(
        {
          nombre,
          descripcion,
        },
        (error, newCategory) => {
          if (error) reject(error);
          resolve(newCategory);
        }
      );
    });
  },
};

module.exports = CategoryController;
