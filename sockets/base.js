const categoryController = require('../controllers/categoryController');
const dishController = require('../controllers/dishController');
const userController = require('../controllers/userController');
module.exports = (io) => {
  io.on('connection', (socket) => {
    categoryController.list().then(
      (success) => {
        socket.emit('ListarCategorias', {
          success: true,
          data: success,
        });
      },
      (error) => {
        socket.emit('ListarCategorias', {
          success: false,
          message: error.message,
        });
      }
    );

    dishController.list().then(
      (success) => {
        socket.emit('ListarPlatos', {
          success: true,
          data: success,
        });
      },
      (error) => {
        socket.emit('ListarPlatos', {
          success: false,
          message: error.message,
        });
      }
    );
    userController.list().then(
      (success) => {
        socket.emit('ListarUsuarios', {
          success: true,
          data: success,
        });
      },
      (error) => {
        socket.emit('ListarUsuarios', {
          success: false,
          message: error.message,
        });
      }
    );
  });
};
