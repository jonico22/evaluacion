var express = require('express');
var router = express.Router();

const categoryController = require('../controllers/categoryController');
/* GET users listing. */

router.get('/', (req, res) => {
  categoryController.list().then(
    (success) => {
      res.json({
        success: true,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        success: false,
      });
    }
  );
});

router.post('/', (req, res) => {
  const nombre = req.body.nombre;
  const descripcion = req.body.descripcion;

  categoryController.store(nombre, descripcion).then(
    (success) => {
      res.json({
        success: true,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        success: false,
      });
    }
  );
});

module.exports = router;
