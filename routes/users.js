var express = require('express');
var router = express.Router();

const userController = require('../controllers/userController');
/* GET users listing. */

router.get('/', (req, res) => {
  userController.list().then(
    (success) => {
      res.json({
        success: true,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        success: false,
      });
    }
  );
});

router.post('/', (req, res) => {
  const nombre = req.body.nombre;
  const apellido_paterno = req.body.apellido_paterno;
  const apellido_materno = req.body.apellido_materno;
  const edad = req.body.edad;
  const email = req.body.email;
  const image = req.body.image;

  userController
    .store(nombre, apellido_paterno, apellido_materno, edad, email, image)
    .then(
      (success) => {
        res.json({
          success: true,
          data: success,
        });
      },
      (error) => {
        res.status(400).json({
          message: error.message,
          success: false,
        });
      }
    );
});
module.exports = router;
