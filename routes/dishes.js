var express = require('express');
var router = express.Router();
const dishController = require('../controllers/dishController');

router.get('/', (req, res) => {
  dishController.list().then(
    (success) => {
      res.json({
        success: true,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        success: false,
      });
    }
  );
});

router.post('/', (req, res) => {
  const categoria_id = req.body.categoria_id;
  const nombre = req.body.nombre;
  const image = req.body.image;
  const precio = req.body.precio;

  dishController.store(categoria_id, nombre, image, precio).then(
    (success) => {
      res.json({
        success: true,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        success: false,
      });
    }
  );
});

module.exports = router;
