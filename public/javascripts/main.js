var socket = io('http://localhost:3500/');
$(document).on('ready', function () {
  let tmp = (name) => {
    return `<div class="text-xs mr-2 my-1 uppercase tracking-wider border px-2 text-indigo-600 border-indigo-600 hover:bg-indigo-600 hover:text-indigo-100 cursor-default">
       ${name}</div>`;
  };
  let tmpDish = (name, price) => {
    return `
        <div class="flex flex-col justify-center px-4 py-4 bg-white border border-gray-300 rounded">
          <div>
            <p class="text-3xl font-semibold text-center text-gray-800">${name}</p>
            <p class="text-lg text-center text-gray-500">${price}</p>
          </div>
        </div>
    `;
  };

  socket.on('ListarCategorias', (response) => {
    $('#listCategory').html('');
    if (response.success) {
      response.data.map((item) => {
        $('#listCategory').append(tmp(item.nombre));
      });
    }
  });
  socket.on('ListarPlatos', (response) => {
    $('#listDishes').html('');
    if (response.success) {
      response.data.map((item) => {
        $('#listDishes').append(tmpDish(item.nombre, item.precio));
      });
    }
  });
});
