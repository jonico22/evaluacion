var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var categoryRouter = require('./routes/category');
var dishesRouter = require('./routes/dishes');
const mongoose = require('mongoose');
var app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

mongoose
  .connect('mongodb://127.0.0.1/evaluacion', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log('Mongo Db Connected'))
  .catch((err) => console.error(err.message));

app.use('/', indexRouter);
app.use('/usuarios', usersRouter);
app.use('/categorias', categoryRouter);
app.use('/platos', dishesRouter);

module.exports = app;
